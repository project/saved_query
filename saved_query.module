<?php

use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * @file
 * Contains saved_query.module.
 */

/**
 * Finds a list of entities with saved_query_fields scheduled for refresh.
 *
 * @return array
 *   A multi-dimensional array where first-level keys are entity type IDs,
 *   second-level keys are field machine names and their values are indexed
 *   arrays of entity IDs, scheduled to be refreshed.
 */
function saved_query_find_candidates() {
  $results = [];

  $field_map = \Drupal::entityManager()->getFieldMap();
  foreach ($field_map as $entity_type_id => $field) {
    foreach ($field as $field_name => $info) {
      if ($info['type'] == 'saved_query_field') {

        $table = $entity_type_id . '__' . $field_name;
        $ids = db_query(
          "SELECT entity_id FROM {$table} t " .
          "WHERE t." .$field_name. "_refreshed < (:now - t." .$field_name. "_interval) " .
          "AND t." .$field_name. "_interval > 0 AND t.deleted = 0;",
            array(':now' => time())
        );

        if ($ids) {
          $results[$entity_type_id][$field_name] = $ids->fetchCol();
        }
      }
    }
  }
  return $results;
}

/**
 * Update referenced values for a given entity type, field name and set of IDs.
 *
 * @param string $entity_type_id
 *   The ID of the entity type being updated, such as 'media' or 'node'.
 * @param string $field_name
 *   The name of the field where the query is defined.
 * @param array $ids
 *   An indexed array of IDs that need to be refreshed.
 */
function saved_query_update_references($entity_type_id, $field_name, array $ids) {
  $storage = \Drupal::entityTypeManager()->getStorage($entity_type_id);
  $entities = $storage->loadMultiple($ids);
  /** @var \Drupal\Core\Entity\FieldableEntityInterface $entity */
    foreach ($entities as $entity) {
    saved_query_update_single_entity($entity, $field_name);
  }
}

/**
 * Run the query and update field values on a given entity.
 *
 * Here we do the actual work of loading a query from the saved_query_field,
 * running it, and updating the destination entity_reference field with the
 * results.
 *
 * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
 *   The entity hosting the saved_query field.
 * @param string $field
 *   The saved_query field machine name.
 * @param bool $save
 *  (optional) Whether to save the entity or not. If called during a preSave
 *  scenario, should be set to FALSE. Defaults to TRUE.
 */
function saved_query_update_single_entity(FieldableEntityInterface $entity, $field, $save = TRUE) {
  if ($entity->hasField($field)) {
    /** @var \Drupal\saved_query\Plugin\Field\FieldType\SavedQueryField $query_field */
    $query_field = $entity->get($field)[0];
    $target_field = $entity->getFieldDefinition($field)->getSetting('target_field');

    if ($target_field && $entity->hasField($target_field) && ($query = $query_field->getQuery())) {
      $ids = $query->execute();
      $references = [];

      foreach ($ids as $id) {
        $references[] = ['target_id' => $id];
      }

      if ($references) {
        $entity->set($target_field, $references);
        $entity->{$field}->refreshed = \Drupal::time()->getRequestTime();
        $entity->{$field}->refresh_now = FALSE;
        if ($save) {
          $entity->save();
        }
      }
    }
  }
}

/**
 * Implements hook_cron().
 */
function saved_query_cron() {
  // @todo: Move to a queue.
  $data = saved_query_find_candidates();
  foreach ($data as $entity_type_id => $fields) {
    foreach ($fields as $field_name => $ids) {
      saved_query_update_references($entity_type_id, $field_name, $ids);
    }
  }
}
