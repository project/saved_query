<?php

namespace Drupal\saved_query\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Serialization\Yaml;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Plugin implementation of the 'raw_saved_query_widget' widget.
 *
 * @FieldWidget(
 *   id = "raw_saved_query_widget",
 *   label = @Translation("Raw Query Editor"),
 *   field_types = {
 *     "saved_query_field"
 *   },
 *   multiple_values = FALSE
 * )
 */
class RawSavedQueryWidget extends WidgetBase {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs widget plugin.
   *
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $entity_types = $this->entityTypeManager->getDefinitions();
    $entity_type_options = array();
    foreach ($entity_types as $key => $data) {
      if ($data->getGroup() == 'content') {
        // Lazy for the moment.
        $entity_type_options[$key] = $data->getLabel();
      }
    }

    $element['#type'] = 'fieldset';
    $element['#collapsible'] = TRUE;

    $element['entity_type'] = [
      '#title' => t('Entity type'),
      '#options' => $entity_type_options,
      '#type' => 'select',
      '#required' => TRUE,
      '#default_value' => isset($items[$delta]->entity_type) ? $items[$delta]->entity_type : 'node',
    ];
    $element['conditions'] = [
      '#title' => t('Conditions'),
      '#type' => 'textarea',
      '#default_value' => empty($items[$delta]->conditions) ? NULL : Yaml::encode($items[$delta]->conditions),
    ];
    $element['sorts'] = [
      '#title' => t('Sorts'),
      '#type' => 'textarea',
      '#default_value' => empty($items[$delta]->sorts) ? NULL : Yaml::encode($items[$delta]->sorts),
    ];
    $element['limit'] = [
      '#title' => t('Result limit'),
      '#type' => 'number',
      '#default_value' => isset($items[$delta]->limit) ? $items[$delta]->limit : NULL,
    ];

    $intervals = [
      (360) => t('1 hour'),
      (360 * 2) => t('2 hour'),
      (360 * 6) => t('6 hours'),
      (360 * 24) => t('1 day'),
      (360 * 24 * 7) => t('1 week'),
    ];
    $element['interval'] = [
      '#title' => t('Refresh interval'),
      '#options' => $intervals,
      '#type' => 'select',
      '#default_value' => isset($items[$delta]->interval) ? $items[$delta]->interval : NULL,
    ];
    $element['refresh_now'] = [
      '#title' => t('Refresh on save'),
      '#type' => 'checkbox',
    ];
    $element['refreshed'] = [
      '#title' => t('Last refreshed'),
      '#type' => 'hidden',
      '#value' => isset($items[$delta]->refreshed) ? $items[$delta]->refreshed : NULL,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    // In a more complex widget, this would be responsible for turning
    // incoming form values into a structured array like so:
    //
    // $values['conditions'] = array(
    //   "entity_field" => "value",
    //   "entity_field_2" => array(
    //     "value" => 1,
    //     "operator" => ">",
    //   ),
    //   'or' => array(
    //     "entity_field_a" => "value",
    //     "entity_field_b" => "value",
    //   );
    // );
    //
    // See https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Entity!Query!QueryInterface.php/function/QueryInterface%3A%3Acondition/8.2.x
    // For details on valid field syntax. You can do lots of crazy stuff.
    //
    // AND / OR conditions are not yet handled, but won't confuse the query builder.
    // Sorts are handled similarly, though the structure is simpler:
    //
    // $values['sorts'] = array(
    //   "entity_field" => "ASC",
    //   "entity_field_2" => "DESC"
    // );
    //
    // ORDER BY RANDOM() isn't supported, because you're a bad person.

    foreach ($values as &$item) {
      foreach ($item as $key => $value) {
        if (empty($value)) {
          unset($item[$key]);
        }
        elseif (in_array($key, ['conditions', 'sorts'])) {
          $item[$key] = Yaml::decode($value);
        }
      }
    }

    return $values;
  }

}
